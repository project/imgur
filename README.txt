CKEditor Imgur Addon

Installation
============

This module requires the core CKEditor module and also the panel button module.

1:- Download the plugin and install it to your drupal installation.
2:- Enable Imgur module in the Drupal admin.
3:- Configure your CKEditor toolbar to include the button.